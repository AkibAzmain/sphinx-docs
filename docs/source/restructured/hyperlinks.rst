
Hyperlinks
----------
* External Links
* Internal Links (Cross referencing locations)
    * Implicit Links to Title
    * Explicit Links

.. sidebar:: External Links

    `<http://www.sphinx-doc.org>`_

    `Sphinx Documentation <http://www.sphinx-doc.org>`_

    `sphinx docs`_
        .. _sphinx docs: http://www.sphinx-doc.org

**External Links**
::

    `<http://www.sphinx-doc.org>`_

   	`Sphinx Documentation <http://www.sphinx-doc.org>`_
    (space between label and link name)

    `sphinx docs`_
   	    .. _sphinx docs: http://www.sphinx-doc.org

|

.. sidebar:: Internal Links

    `HyperLinks`_ *(Implicit Link to Title)*

    :ref:`overview` *(Explicit Links)*

    :ref:`index`

**Implicit Links to Title**
*(if title and links are within same rst file)*::

    `Hyperlinks`_

**Explicit Links**
*(label is to be defined at the top of documentation)*::

    .. _overview:

Label can be referred in two ways:
::

    overview_ (link appeared as a label name)

    :ref:`overview` (link appeared as a Title Name, 'ref' is used to link external RST file)

