Render PDF from RST
===================
#. Latex (install a full latex stack, latexmk package)
#. rst2pdf

Latex
-----
::

    $ sudo apt install texlive-latex-base
    $ apt-get install texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended

::

    $ sudo apt install latexmk
    $ pdflatex project_name

**Generate latex:**
::

    $ sphinx-build -b latex sourcedir builddir/latex or,
    $ make latexpdf

rst2pdf
-------
::

    $ sudo pip install rst2pdf or,
    $ sudo easy_install rst2pdf

Add an extension and pdf_documents to **conf.py**
::

    extensions = ['rst2pdf.pdfbuilder']

::

    pdf_documents = [('index', u'pdf name', u'Sample doc Title', u'author name')]


**Generate PDF**
::

    $ sphinx-build -b pdf sourcedir builddir/pdf

