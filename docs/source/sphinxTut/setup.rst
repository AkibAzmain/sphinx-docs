
Setup and Installations
=======================

**Install Python**
::

    $ apt-get install python-sphinx python-setuptools
    $ pip install --upgrade python
    $ python --version

**Install pip**
::

    $ easy_install pip or,
    $ apt install python-pip

    $ pip install --upgrade pip
    $ pip --version

**Install Sphinx using pip**
::

    $ pip install -U sphinx /

    $ pip install sphinx --upgrade
    $ pip install sphinx sphinx-autobuild sphinx_rtd_theme sphinxcontrib-phpdomain

**Install PHP Domain**
::

    $ sudo pip install sphinxcontrib-phpdomain or,
    $ sudo easy_install -U sphinxcontrib-phpdomain

*Also, add an extension to conf.py*
::

    extensions = ["sphinxcontrib.phpdomain"]


