Getting Started
===============

Create Sphinx Project
---------------------

.. code-block:: shell

   $ mkdir docs-sample
   $ cd docs-sample

   $ sphinx-quickstart

* *(generates a documentation source directory and populate it with some defaults-project name, user name, version, extension(.rst preferred))*

.. code-block:: shell

   $ make html

**toctree**

* way to connect multiple files to a single hierarchy of documents
* list the items of table of content(toc) just below :maxdepth, after a blank lines.
* title appeared in toctree are file's title
* maxdepth is used to indicates the depth of the tree.
* toctree is automatically mirrored in the left ToC navigation bar.

.. code-block:: rest

    .. toctree::
       :maxdepth: 2

       content1
       content2

**glob:** glob flag option can be used in toctree. * and ? characters are used to indicate patterns.
Entries are then matched against the list of available documents and are inserted alphabetically.
Also used to make title more meaningful.

.. code-block:: rest

    .. toctree::
       :glob:

       Content 1 <content1>

**content directive:** generates a table of contents (TOC) in a topic. 'depth' indicates the max section depth to be shown in the contents

.. code-block:: rest

    .. contents:: Table of Contents
       :depth: 2

Running the build
-----------------

.. sidebar:: Note

   * **root or source directory :** collection of reStructuredText document sources
   * **build directory :** collection of compiled html documents
   * **conf.py :** configuration file
   * **index.rst :** master document, that serves as a welcome page, contains the root of toctree (table of contents)

.. code-block:: shell

   $ sphinx-build -b html sourcedir builddir

* **-b:** *selects a builder (e.g. html, dirhtml, singlehtml, text, xml)*
* **sourcedir:** *source directory*
* **builddir:** *directory to place the built documentation*

.. code-block:: shell

   $ make html

* *analyse the files in the **source/** directory and create the HTML files into the directory **_build/html/** or **build/html** depending on configuration file*

.. code-block:: shell

   $ make clean

* *clears everything within build/ directory*

Compile the documentation
-------------------------
*($ make - command)*

.. code-block:: shell

    $ make html
    $ make latex    [Linux]
    $ make.bat html [Windows]











