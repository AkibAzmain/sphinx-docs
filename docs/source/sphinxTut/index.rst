.. _overview:

========
Overview
========

**Sphinx** is a tool that makes it easy to create intelligent and beautiful documentation.
It has excellent facilities for the documentation of software projects in a range of languages
and is created from reStructuredText sources.

**reStructuredText**

* Default plaintext markup language used by Sphinx
* RST or ReST (Restructed Text), human readable nature, that makes it useful to include our source code
* Can also be interpreted into pdf, html
* Provides an easy-to-read, what-you-see-is-what-you-get plaintext markup syntax and parser system
* RST syntax is sensitive to indentation ! and requires blank lines between paragraphs

*(Sphinx uses reStructuredText as its markup language. Many of its strengths come from the power and straightforwardness of reStructuredText and its parsing and translating suite, the Docutils).*

**readthedocs.org** hosts a lot of Sphinx docs already, and integrates well with projects' source control.
It also features a powerful built-in search that exceeds the possibilities of Sphinx' JavaScript-based offline search.

.. include:: setup.rst

Themes
======
Sphinx supports changing the appearance of its HTML output via themes.
A theme is a collection of HTML templates, stylesheets.

**readthedocs theme**
::

   $ pip install sphinx_rtd_theme

Add an extension to **conf.py**::

   html_theme = "sphinx_rtd_theme"

Read more: `Sphinx - HTML Theming support <http://www.sphinx-doc.org/en/master/theming.html>`_
